#include <Arduino.h>

#include "rf_ledc.h"
#include "rf_rgb.h"

RfRgb::RfRgb(double freq, uint8_t res, uint16_t maxValue, float gamma) : m_freq(freq), m_res(res), m_maxValue(maxValue), m_gamma(gamma)
{
}

RfRgb::~RfRgb()
{
    if(ledR) delete ledR;
    if(ledG) delete ledG;
    if(ledB) delete ledB;
}

void RfRgb::init(int gpioR, uint8_t channelR, int gpioG, uint8_t channelG, int gpioB, uint8_t channelB)
{
    ledR = new RfLedc(gpioR, channelR, m_freq, m_res, m_maxValue, m_gamma);
    ledG = new RfLedc(gpioR, channelR, m_freq, m_res, m_maxValue, m_gamma);
    ledB = new RfLedc(gpioR, channelR, m_freq, m_res, m_maxValue, m_gamma);
}

void RfRgb::setRgb(uint16_t r, uint16_t g, uint16_t b)
{
    ledR->setValue(r);
    ledG->setValue(g);
    ledB->setValue(b);
}

void RfRgb::setGamma(float gamma)
{
    ledR->setGamma(gamma);
    ledR->setValue(ledR->value());
    ledG->setGamma(gamma);
    ledG->setValue(ledG->value());
    ledB->setGamma(gamma);
    ledB->setValue(ledB->value());
}

float RfRgb::gamma() const
{
    return m_gamma;
}

uint16_t RfRgb::valueR() const
{
    return ledR->value();
}

uint16_t RfRgb::valueG() const
{
    return ledG->value();
}

uint16_t RfRgb::valueB() const
{
    return ledB->value();
}
