#ifndef _RF_RGB_H
#define _RF_RGB_H

#include "rf_ledc.h"

class RfRgb
{
public:
    RfRgb(double freq, uint8_t res, uint16_t maxValue, float gamma);
    ~RfRgb();

    void init(int gpioR, uint8_t channelR, int gpioG, uint8_t channelG, int gpioB, uint8_t channelB);
    void setRgb(uint16_t r, uint16_t g, uint16_t b);
    void setGamma(float gamma);

    float gamma() const;
    uint16_t valueR() const;
    uint16_t valueG() const;
    uint16_t valueB() const;

private:
    double m_freq;
    uint8_t m_res;
    uint16_t m_maxValue;
    float m_gamma;

    RfLedc *ledR = nullptr;
    RfLedc *ledG = nullptr;
    RfLedc *ledB = nullptr;
};

#endif
